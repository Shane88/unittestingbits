# Database Testing Framework #

**Description:**
Flexible UseDatabaseAttribute like mechanism

**Features:**

- Read from App.Config or external config such as Assembly level attribute with config file path (like log4net). 
- Possibly a Fluent API alternative
- Multiple database configurations can be used. Each database configuration can have the following configured
	- database setup e.g. one or more scripts or a dacpac
	- database names
	- connection strings 
	- Database type names (e.g. System.Data.SqlClient or their ConnectionFactory equivalents). This allows users to provide their own factory types here to do things such as wrapping connections with interceptors
	- Additional free flow settings that can be passed to the Database Connection Factories so they can perform setup
	- Server setup options (e.g. spawn new localdb server instance)
		- Type name for Server setup class to use
		- additional free flow settings that can be passed to Server Setup class so it can perform setup (e.g. server names, file locations)
	- Running one or more scripts before each test
	- Running one or more scripts after each test
	- Running pre deployment script from dacpac before each test
	- Running post deployment script from dacpac after each test
- Transactional Unit Tests/All modified data is rolled back after test.
- Injecting convenience connection details into test classes/test context such as connection factories and connection strings. Could just be a static method call such as TestDatabase.GetNewConnection()
- Support for MSTest, NUnit, XUnit, SpecFlow to be considered up front to help craft the API to allow for various testing frameworks


**Namespace ideas**

- UnitTestingBits.Databases
- UnitTestingBits.Databases.NUnit
- UnitTestingBits.Databases.SpecFlow etc