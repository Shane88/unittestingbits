﻿namespace UnitTestingBits.TestsRunner
{
   using System.Linq;
   using FluentAssertions;
   using UnitTestingBits.Databases;
   using UnitTestingBits.Databases.TestAssists;
   using UnitTestingBits.Databases.XUnit.Fody;
   using Xunit;
   using Xunit.Abstractions;

   public class XUnit_WeavedStructure_Tests
   {
      [Fact]
      public void Empty_classes_with_usesdatabasesattribute_should_receive_correct_structure()
      {
         // Arrange.

         // Act.
         var testHelper =
            new ModuleWeaverTestHelper<ModuleWeaver>("XUnitAssemblyWithDefaultProvider.dll");

         // Assert.
         testHelper.Errors.Count.Should().Be(0);
         var type = testHelper.ModuleDefinition.GetType("XUnitAssemblyWithDefaultProvider.EmptyClassWithDefaultProvider");
         type.HasDisposeMethod().Should().BeTrue();

         var ctor = type.Methods.Single(m => m.IsConstructor);
         ctor.Parameters.Count.Should().Be(2);
         ctor.Parameters.First().ParameterType.FullName.Should().Be(typeof(DatabaseFixture).FullName);
         ctor.Parameters.Last().ParameterType.FullName.Should().Be(typeof(ITestOutputHelper).FullName);
      }

      [Fact]
      public void Empty_classes_with_connection_attributes_only_should_receive_correct_structure()
      {
         // Arrange.

         // Act.
         var testHelper =
            new ModuleWeaverTestHelper<ModuleWeaver>("XUnitAssemblyWithDefaultProvider.dll");

         // Assert.
         testHelper.Errors.Count.Should().Be(0);
         var type = testHelper.ModuleDefinition.GetType("XUnitAssemblyWithDefaultProvider.ClassWithConnectionButNoUsesDatabases");
         type.HasDisposeMethod().Should().BeTrue();

         var ctor = type.Methods.Single(m => m.IsConstructor);
         ctor.Parameters.Count.Should().Be(2);
         ctor.Parameters.First().ParameterType.FullName.Should().Be(typeof(DatabaseFixture).FullName);
         ctor.Parameters.Last().ParameterType.FullName.Should().Be(typeof(ITestOutputHelper).FullName);
      }
   }
}