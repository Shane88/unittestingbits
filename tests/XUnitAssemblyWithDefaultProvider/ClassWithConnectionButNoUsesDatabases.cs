﻿namespace XUnitAssemblyWithDefaultProvider
{
   using System.Data;
   using UnitTestingBits.Databases.Contracts;

   public class ClassWithConnectionButNoUsesDatabases
   {
      [Connection]
      public IDbConnection Database1Connection { get; set; }
   }
}