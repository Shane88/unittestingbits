﻿namespace XUnitAssemblyWithDefaultProvider
{
   using UnitTestingBits.Databases.Contracts;

   [UsesDatabases]
   public class EmptyClassWithDefaultProvider
   {
   }
}